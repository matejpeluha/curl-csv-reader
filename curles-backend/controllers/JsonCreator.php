<?php
require_once("DatabaseCommunicator.php");

class JsonCreator
{
    private array $json;

    public function __construct(){
        $this->json = [];
    }

    public function getJson(): bool|string
    {
        $this->loadAllRecords();

        return json_encode($this->json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    private function loadAllRecords(){
        $databaseCommunicator = new DatabaseCommunicator();
        $this->json = $databaseCommunicator->getAllRecords();
    }


}