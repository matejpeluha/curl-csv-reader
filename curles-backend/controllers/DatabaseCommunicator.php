<?php
require_once("entities/Attendance.php");
require_once("config.php");


class DatabaseCommunicator
{
    private PDO $connection;
    public function __construct(){
        $this->connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function isCsvInDatabase($csvName): bool{
        try {
            $query = $this->connection->prepare("SELECT * FROM attendances WHERE attendances.lecture_name=:lecture_name LIMIT 1");
            $bindParameters = [':lecture_name' => $csvName];
            $query->execute($bindParameters);

            $query->setFetchMode(PDO::FETCH_CLASS, "Attendance");
            $allAttendances = $query->fetchAll();

            if (!$allAttendances || sizeof($allAttendances) < 1)
                return false;
        }
        catch (Exception $exception){
            return false;
        }
        return true;
    }

    public function deleteMissingCsvFromDatabase($allCsvNames){
        $queryStatement = $this->getSqlDeleteQuery($allCsvNames);

        try {
            $query = $this->connection->prepare($queryStatement);
            $query->execute();
        }
        catch (Exception $exception){
            echo "chyba";
        }
    }

    private function getSqlDeleteQuery($allCsvNames): string
    {
        $whereStatement = "WHERE ";
        for($index = 0; $index < sizeof($allCsvNames); $index++){
            $whereStatement .= "lecture_name!='" . $allCsvNames[$index] ."'";
            if($index != sizeof($allCsvNames) - 1)
                $whereStatement .= " AND ";
        }

        return "DELETE FROM attendances " .  $whereStatement;
    }

    public function insertCsvToDatabase($csvName, $csv){
        unset($csv[0]);
        try {
            $query = $this->connection->prepare("INSERT INTO attendances(lecture_name, name, action, time) 
                                                      VALUES(?, ?, ?, ?)");

            foreach ($csv as $item) {
                $cleanTime =  str_replace(" AM", "", $item[2]);
                $bindParameters = [$csvName, $item[0], $item[1], $cleanTime];
                $query->execute($bindParameters);
            }
        }
        catch (Exception $exception){

        }
    }

    public function getAllRecords(): array
    {
        $allPeople = $this->getAllPeople();
        $allRecords = [];
        for ($index = 0; $index < sizeof($allPeople); $index++) {
            $person = $allPeople[$index];
            $person->setId($index);
            array_push($allRecords, $this->getOneRecord($person));
        }

        $graphStats = $this->getGraphStats();
        $result = [ "graphStats" => $graphStats, "records" => $allRecords];
        return $result;
    }

    private function getGraphStats(): array
    {
        $lectureNames = $this->getEmptyLectures();

        $graphStats = [];
        foreach ($lectureNames as $lecture){
            $lectureAttendance = $this->getLectureAttendance($lecture);
            $date = $this->getLectureDateFromTimeStamp($lecture->getLectureName());
            $graphStats[] = ["lectureName" => $lecture->getLectureName(), "date" => $date,
                                "count" => $lectureAttendance];
        }


        return $graphStats;
    }


    private function getLectureAttendance($lecture): int
    {
        $query = $this->connection->prepare("SELECT attendances.name FROM attendances 
                                                    WHERE attendances.lecture_name=:lecture_name 
                                                    GROUP BY attendances.name");
        $bindParameters = [':lecture_name' => $lecture->getLectureName()];
        $query->execute($bindParameters);

        $query->setFetchMode(PDO::FETCH_CLASS, "Attendance");
        $allStudents = $query->fetchAll();

        return sizeof($allStudents);
    }

    private function getOneRecord($person){
        $fullName = $person->getFullName();
        $lectures = $this->getLectures($fullName);

        return ["id" => $person->getId(),"name" => $person->getName(), "surname" => $person->getsurname(),
                "lectures" => $lectures];
    }

    private function getAllPeople(): array
    {
        $query = $this->connection->prepare("SELECT attendances.name FROM attendances GROUP BY attendances.name");
        $query->execute();

        $query->setFetchMode(PDO::FETCH_CLASS, "Attendance");
        $allPeople = $query->fetchAll();

        return $allPeople;
    }

    private function getEmptyLectures(): array
    {
        $query = $this->connection->prepare("SELECT attendances.lecture_name FROM attendances 
                                                    GROUP BY attendances.lecture_name");
        $query->execute();

        $query->setFetchMode(PDO::FETCH_CLASS, "Attendance");
        $allLectures = $query->fetchAll();

        return $allLectures;
    }

    private function getLectures($fullName): array
    {
        $allLectures = $this->getEmptyLectures();

        $allLectureNames = [];
        foreach ($allLectures as $lecture) {
            array_push($allLectureNames, $this->getLectureRecords($fullName, $lecture));
        }

        return $allLectureNames;
    }

    private function getLectureRecords($fullName, $lecture): array
    {
        $lectureName = $lecture->getLectureName();
        $endTime = $this->getLectureEndTime($lectureName);
        $actions = $this->getActions($fullName, $lectureName);
        $attendanceTime = $this->getAttendanceTimeInLecture($actions, $endTime);
        $date = $this->getLectureDateFromTimeStamp($lectureName);
        $isFinished = $this->isFinished($actions);

        return [ "lecture_name" => $lectureName, "date" => $date,"finished" => $isFinished,"lectureEndTime" => $endTime,
            "attendanceTime" => $attendanceTime, "actions" => $actions];
    }

    private function isFinished($actions): bool
    {
        if(sizeof($actions) < 1)
            return true;

        $lastActionsIndex = sizeof($actions) - 1;
        $lastAction = $actions[$lastActionsIndex];
        $lastActionType = $lastAction["type"];

        if(strtolower($lastActionType) === "left" )
            return true;
        else
            return false;

    }

    private function getLectureDateFromTimeStamp($lectureName){
        $date = explode("_", $lectureName)[0];
        $year = substr($date, 0, 4);
        $month = substr($date, 4, 2);
        $day = substr($date, 6, 2);

        return $day . "/" . $month . "/" . $year;
    }

    private function getLectureEndTime($lectureName){
        $query = $this->connection->prepare("SELECT MAX(attendances.time) AS time 
                                                    FROM attendances WHERE attendances.lecture_name=:lecture_name AND
                                                        attendances.action='Left'");
        $bindParameters = [':lecture_name' => $lectureName];
        $query->execute($bindParameters);

        $query->setFetchMode(PDO::FETCH_CLASS, "Attendance");
        $allAttendances = $query->fetchAll();

        return $allAttendances[0]->getTime();
    }

    private function getActions($name, $lectureName): array
    {
        $query = $this->connection->prepare("SELECT attendances.action, attendances.time
                                                    FROM attendances WHERE attendances.lecture_name=:lecture_name AND
                                                    attendances.name=:name");
        $bindParameters = [':lecture_name' => $lectureName, ':name' => $name];
        $query->execute($bindParameters);

        $query->setFetchMode(PDO::FETCH_CLASS, "Attendance");
        $allAttendances = $query->fetchAll();

        $allActions = [];
        foreach ($allAttendances as $attendance){
            $allActions[] = ["type" => $attendance->getAction(), "time" => $attendance->getTime()];
        }

        return $allActions;
    }

    private function getAttendanceTimeInLecture($actions, $lectureEndTime)
    {
        $attendanceTime = 0;
        $lectureEndTimeStamp = $this->getTimeStamp($lectureEndTime);
        for($index = 0; $index < sizeof($actions); $index++){
            $time = $actions[$index]["time"];
             $timeStamp = $this->getTimeStamp($time);
             $actionType = $actions[$index]["type"];

             if(strtolower($actionType) === "joined"){
                 $attendanceTime -= $timeStamp;
             }
             elseif (strtolower($actionType) === "left"){
                 $attendanceTime += $timeStamp;
             }

             if($index === sizeof($actions) - 1 && strtolower($actionType) === "joined"){
                 if($timeStamp > $lectureEndTimeStamp)
                     $lectureEndTime = $time;

                 array_push($actions, ["type" => "Left", "time" => $lectureEndTime]);
             }
        }

        if($attendanceTime < 0)
            $attendanceTime = 0;

        return $attendanceTime;
    }

    public function getTimeStamp($time): int
    {
        $dateTime = DateTime::createFromFormat('d/m/Y, H:i:s', $time);
        if($dateTime === false)
            return 0;
        else
            return $dateTime->getTimestamp();
    }
}