<?php
require_once("DatabaseCommunicator.php");


class DataDownloader
{
    private string $destination;
    private string $repoUrl;

    public function __construct()
    {
        $this->repoUrl = "https://api.github.com/repos/apps4webte/curldata2021/contents";
        //$this->repoUrl = "https://api.github.com/repos/matejpeluha/webte2-attendance/contents";
    }

    public function updateData(){
        $data = $this->getDataFromUrl($this->repoUrl);
        $content = json_decode($data);

        $this->mergeWithDatabase($content);
    }

    private function mergeWithDatabase($content){
        foreach ($content as $file) {
            $this->downloadData($file);
        }

        $this->deleteMissingFromDatabase($content);
    }

    private function deleteMissingFromDatabase($content){
        $allFileNames = [];
        foreach ($content as $file) {
            $allFileNames[] = $file->name;
        }

        $databaseCommunicator = new DatabaseCommunicator();
        $databaseCommunicator->deleteMissingCsvFromDatabase($allFileNames);
    }

    private function downloadData($file)
    {
        $output = $this->getDataFromUrl($file->download_url);
        $output = mb_convert_encoding($output, 'UTF-8', 'UTF-16LE');

        $lines = explode(PHP_EOL, $output);
        $csv = [];

        foreach ($lines as $line)
            $csv[] = str_getcsv($line, "\t");
        unset($csv[0]);

        $this->addToDatabase($file->name, $csv);

    }

    private function addToDatabase($csvName, $csv){
         $databaseCommunicator = new DatabaseCommunicator();
         if(!$databaseCommunicator->isCsvInDatabase($csvName)){
             $databaseCommunicator->insertCsvToDatabase($csvName, $csv);
         }
    }

    private function getDataFromUrl($url): bool|string
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "apps4webte");
        //curl_setopt($ch, CURLOPT_USERAGENT, "matejpeluha");

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }
}