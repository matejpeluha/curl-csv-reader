<?php


class Attendance
{
    private $id;
    private $lecture_name;
    private $name;
    private $action;
    private $time;


    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLectureName()
    {
        return $this->lecture_name;
    }

    /**
     * @return mixed
     */
    public function getFullName(){
        return $this->name;
    }

    public function getName()
    {
        $first_name = explode(" ", $this->name)[0];
        return $first_name;
    }

    public function getSurname(){
        $nameArray = explode(" ", $this->name);
        $surname = "";

        for ($index = 1; $index < sizeof($nameArray); $index++){
            $surname .= $nameArray[$index];
            if($index != sizeof($nameArray) - 1)
                $surname .= " ";
        }

        return $surname;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    public function getTimeStamp(): int
    {
        $dateTime = DateTime::createFromFormat('d/m/Y, H:i:s', $this->time);
        if($dateTime === false)
            return 0;
        else
            return $dateTime->getTimestamp();
    }

}