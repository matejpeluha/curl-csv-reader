window.addEventListener("load", function (){
    let reqHeader = new Headers();
    reqHeader.append('Content-Type', 'application/json');
    let initObject = {
        method: 'POST', headers: reqHeader,
    };

    fetch('curlHandler.php', initObject)
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            console.log(data);
        })
        .catch(function (err) {
            console.log("Something went wrong!", err);
        });


})