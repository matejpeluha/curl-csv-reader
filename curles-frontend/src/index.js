import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import TablePage from "./components/TablePage";

ReactDOM.render(
  <React.StrictMode>
      <Router basename={"/curls"}>
          <Switch>
              <Route path={""} component={TablePage}>
              </Route>
          </Switch>
      </Router>
  </React.StrictMode>,
  document.getElementById('root')
);
