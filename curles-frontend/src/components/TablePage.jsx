import React, {Component} from 'react';
import AttendanceTable from "./AttendanceTable";
import LectureModal from "./LectureModal";
import Graph from "./Graph";
import "../components-css/tablePage.css"
import LoadingScreen from "./LoadingScreen";


//rcjc shortcut

class TablePage extends Component {
    state = {
        "loading": true,
        "hidden": "graph",
        "modal":{
            "isOpened": false,
            "personName": "",
            "lectureIndex": 0,
            "lecture": {
                "date": "16/02/2021",
                "attendanceTime": 0,
                "lectureEndTime": "",
                "actions": []
            }
        },
        "graphStats": [

        ]
    }

    render() {
        const tableVisibility = this.getClassVisibility("table");
        const graphVisibility = this.getClassVisibility("graph");
        const tableButtonActivity = this.getActiveButtonClass("table");
        const graphButtonActivity = this.getActiveButtonClass("graph");

        return (
            <main>
                <LoadingScreen active={this.state.loading}/>
                <h1>Dochádzka na Webte2</h1>
                <div className={"button-container"}>
                    <button className={tableButtonActivity} onClick={() => this.hide("graph")}>Tabuľka</button>
                    <button className={graphButtonActivity} onClick={() => this.hide("table")}>Graf</button>
                </div>

                <div className={tableVisibility} >
                    <AttendanceTable
                        onShowModal={this.handleShowModal}
                        onJsonLoaded={this.handleJsonLoaded}
                        onLoadingFinished={this.stopLoading}
                    />
                </div>
                <div className={graphVisibility}>
                    <Graph stats={this.state.graphStats} />
                </div>
                <LectureModal modal-info={this.state.modal} onCloseModal={this.handleCLoseModal}/>

                <footer id={"page-footer"}>React <i className="fab fa-react"/> Matej Peluha</footer>
            </main>
        );
    }

    getActiveButtonClass(property){
        const hidden = this.state.hidden;
        if(hidden !== property)
            return "active-button";

        return "";
    }

    stopLoading = () =>{
        this.setState({
            "loading": false,
            "modal": this.state.modal,
            "graphStats": this.state.graphStats,
            "hidden": this.state.hidden
        })
    }

    getClassVisibility(property){
        const hidden = this.state.hidden;
        if(hidden === property)
            return "hide";

        return "";
    }

    hide = property =>{
        this.setState({
            "loading": this.state.loading,
            "modal": this.state.modal,
            "graphStats": this.state.graphStats,
            "hidden": property
        })
    }


    handleJsonLoaded = (graphStats) =>{
        this.setState({
            "loading": this.state.loading,
            "modal": this.state.modal,
            "graphStats": graphStats,
            "hidden": this.state.hidden
        })
    }

    handleCLoseModal = () =>{
        const modalInfo = {
            "isOpened": false,
            "personName": "",
            "lectureIndex": 0,
            "lecture": {
                "date": "16/02/2021",
                "attendanceTime": 0,
                "lectureEndTime": "",
                "actions": []
            }
        }


        const newState = {
            "loading": this.state.loading,
            "modal": modalInfo,
            "graphStats": this.state.graphStats,
            "hidden": this.state.hidden
        };
        this.setState(newState);
    }

    handleShowModal = (person, lectureIndex) =>{
        const lecture = person["lectures"][lectureIndex];
        const modalInfo = {
            "isOpened": true,
            "personName": person["name"] + " " + person["surname"],
            "lectureIndex": lectureIndex + 1,
            "lecture": lecture
        }


        const newState = {
            "loading": this.state.loading,
            "modal": modalInfo,
            "graphStats": this.state.graphStats,
            "hidden": this.state.hidden
        };
        this.setState(newState);
    }
}

export default TablePage;
