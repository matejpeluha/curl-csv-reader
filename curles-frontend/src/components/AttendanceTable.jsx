import  React, {Component} from 'react';
import "../components-css/attendanceTable.css"

class AttendanceTable extends Component {
    state = {
        "timeSort": 0,
        "countSort": 0,
        "nameSort": 0,
        "search": "",
        "records": []};

    constructor(props) {
        super(props);
        this.loadState().then(response => this.props.onLoadingFinished());
    }

    render() {
        return (
            <div>
                <div id={"input-root"}>
                    <input id={"name-finder"} placeholder={"Vyhľadaj meno"} type={"text"} onInput={this.handleInput}/>
                </div>
                <div id={"table-root"}>
                    <table>
                        {this.getTableHeader()}
                        {this.getTableBody()}
                    </table>
                </div>
            </div>
        );
    }

    handleInput = () =>{
        const searchedName = document.getElementById("name-finder").value;

        this.setState({
            timeSort: this.state.timeSort,
            countSort: this.state.countSort,
            nameSort: this.state.nameSort,
            search: searchedName,
            records: this.state.records
        });
    }

    getTableHeader(){
        return(
            <thead>
            <tr id={"tr-header"}>
                <th id={"name-th"} onClick={this.sortSurname}>
                    Meno {this.getSortIcon(this.state.nameSort)}
                </th>
                {this.getLectureHeaders()}
                <th id={"count-th"} onClick={this.sortAttendanceCount}>
                    Účasti {this.getSortIcon(this.state.countSort)}
                </th>
                <th id={"time-th"} onClick={this.sortTime}>
                    Čas {this.getSortIcon(this.state.timeSort)}
                </th>
            </tr>
            </thead>
        );
    }

    getSortIcon = (sort) => {
        if (sort === 1)
            return <i className="fas fa-sort-down"/>;
        else if (sort === -1)
            return <i className="fas fa-sort-up"/>;
        else
            return <i className="fas fa-sort"/>;
    }


    compareIDs = (a, b) =>{
        const idA = a["id"];
        const idB = b["id"];

        return idA - idB;
    }

    sortSurname = () => {
        const allRecords = this.state["records"];
        const nameSort = this.changeNameSort()

        this.setState({
            timeSort: 0,
            countSort: 0,
            nameSort: nameSort,
            search: this.state.search,
            records: allRecords
        }, this.startSurnameSort);
    }

    startSurnameSort = () =>{
        const allRecords = this.state["records"];
        const nameSort = this.state["nameSort"];

        if(nameSort === 0)
            allRecords.sort(this.compareIDs);
        else
            allRecords.sort(this.compareSurnames);

        this.setState({
            timeSort: 0,
            countSort: 0,
            nameSort: nameSort,
            search: this.state.search,
            records: allRecords
        });
    }

    changeNameSort(){
        let nameSort = this.state.nameSort;
        if(nameSort === 0)
            nameSort = -1;
        else if(nameSort === -1)
            nameSort = 1;
        else if(nameSort === 1)
            nameSort = 0;

        return nameSort;
    }

    compareSurnames = (a, b) => {
        const surnameA = a["surname"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        const surnameB = b["surname"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        const nameA = a["name"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        const nameB = b["name"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        const nameSort = this.state["nameSort"];

        if(surnameA > surnameB){
            return nameSort;
        }
        else if(surnameA < surnameB){
            return -1 * nameSort;
        }
        else if(surnameA === surnameB && nameA > nameB){
            return nameSort;
        }
        else if(surnameA === surnameB && nameA < nameB){
            return -1 * nameSort;
        }
        return 0;
    }

    sortAttendanceCount = () => {
        const allRecords = this.state["records"];
        const countSort = this.changeCountSort();

        this.setState({
            timeSort: 0,
            countSort: countSort,
            nameSort: 0,
            search: this.state.search,
            records: allRecords
        }, this.startCountSort);
    }

    startCountSort = () =>{
        const allRecords = this.state["records"];
        const countSort = this.state.countSort;
        if(countSort === 0)
            allRecords.sort(this.compareIDs);
        else
            allRecords.sort(this.compareAttendanceCounts);

        this.setState({
            timeSort: 0,
            countSort: countSort,
            nameSort: 0,
            search: this.state.search,
            records: allRecords
        });
    }

    changeCountSort(){
        let countSort = this.state["countSort"];
        if(countSort === 0)
            countSort = -1;
        else if(countSort === -1)
            countSort = 1;
        else if(countSort === 1)
            countSort = 0;

        return countSort;
    }

    compareAttendanceCounts = (a, b) => {
        let countA = this.getNumberOfAttendedLectures(a["lectures"]);
        let countB = this.getNumberOfAttendedLectures(b["lectures"]);
        const surnameA = a["surname"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        const surnameB = b["surname"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        const countSort = this.state.countSort;

        if(countA > countB){
            return countSort;
        }
        else if(countA < countB){
            return -1 * countSort;
        }

        else if(countA === countB && surnameA > surnameB){
            return countSort;
        }
        else if(countA === countB && surnameA < surnameB){
            return -1 * countSort;
        }

        return 0;
    }

    sortTime = () => {
        const allRecords = this.state["records"];
        const timeSort = this.changeTimeSort();

        this.setState({timeSort: timeSort,
            countSort: 0,
            nameSort: 0,
            search: this.state.search,
            records: allRecords
        }, this.startTimeSort);



    }

    startTimeSort = () => {
        const allRecords = this.state["records"];
        const timeSort = this.state["timeSort"];

        if(timeSort === 0)
            allRecords.sort(this.compareIDs);
        else
            allRecords.sort(this.compareTimes);

        this.setState({timeSort: timeSort,
            countSort: 0,
            nameSort: 0,
            search: this.state.search,
            records: allRecords});
    }

    changeTimeSort(){
        let timeSort = this.state.timeSort;
        if(timeSort === 0)
            timeSort = -1;
        else if(timeSort === -1)
            timeSort = 1;
        else if(timeSort === 1)
            timeSort = 0;

        return timeSort;
    }

    compareTimes = (a, b) =>{
        let secondsA = this.getTotalSeconds(a["lectures"]);
        let secondsB = this.getTotalSeconds(b["lectures"]);

        const surnameA = a["surname"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        const surnameB = b["surname"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");


        if(this.state.timeSort === 1 && secondsA > secondsB){
            return 1;
        }
        else if(this.state.timeSort === 1 && secondsA < secondsB){
            return -1;
        }
        else if(this.state.timeSort === -1 && secondsA > secondsB){
            return -1;
        }
        else if(this.state.timeSort === -1 && secondsA < secondsB){
            return 1;
        }
        else if(this.state.timeSort === 1 && secondsA === secondsB && surnameA > surnameB){
            return 1;
        }
        else if(this.state.timeSort === 1 && secondsA === secondsB && surnameA < surnameB){
            return -1;
        }
        else if(this.state.timeSort === -1 && secondsA === secondsB && surnameA > surnameB){
            return -1;
        }
        else if(this.state.timeSort === -1 && secondsA === secondsB && surnameA < surnameB){
            return 1;
        }
        return 0;
    }

    getLectureHeaders(){
        let lectureHeaders = [];
        if(this.state.records.length < 1)
            return lectureHeaders;

        const lectures = this.state.records[0]["lectures"];
        for (let index = 0; index < lectures.length; index++) {
            lectureHeaders.push(<th key={"l-" + index}>{index + 1}.<br/>{lectures[index]["date"]}</th>);
        }

        return lectureHeaders;
    }

    getTableBody(){
        return(
            <tbody>
                {this.state.records.map( (person, key) => this.getTableRow(person, "p-" + key) )}
            </tbody>
        );
    }


    getTableRow(person, key){
        if(!this.isPersonSearched(person))
            return ;


        return(
            <tr key={key}>
                {this.getNameCell(person)}
                {this.getLecturesCells(person, key)}
                {this.getNumberOfAttendedLecturesCell(person["lectures"])}
                {this.getTotalTimeCell(person["lectures"])}
            </tr>
        );
    }

    isPersonSearched(person){
        const name = person["name"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        const surname = person["surname"].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        const fullNameLeft = name + " " + surname;
        const fullNameRight = surname + " " + name;
        const searched = this.state.search.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");

        return name.startsWith(searched) || surname.startsWith(searched)
            || fullNameLeft.startsWith(searched) || fullNameRight.startsWith(searched);
    }

    getNameCell(person){
        return(
            <td className={"name-td"}>
                <p className={"surname"}>{person["surname"] + ", "}</p>
                <span className={"name"}>{" " + person["name"]}</span>
            </td>
        );
    }

    getLecturesCells(person, rowKey){
        const lectures = person["lectures"]
        const lecturesCells = [];

        for (let index = 0; index < lectures.length; index++) {
            let time = "00:00:00";
            const lecture = lectures[index];
            const seconds = lecture["attendanceTime"];

            const date = new Date(seconds * 1000);
            date.setHours(date.getHours() - 1);
            time = date.toTimeString().split(' ')[0];

            const finishedClass = this.getFinishedLectureClass(lecture);

            const cell = <td
                            className={"lecture-td " + finishedClass}
                            key={rowKey + "-l-" + index}
                            onClick={() => this.props.onShowModal(person, index)}
                            >{time}</td>
            lecturesCells.push(cell);
        }

        return lecturesCells;
    }

    getFinishedLectureClass(lecture){
        if(lecture["finished"] === false)
            return "not-finished";
        else
            return "";
    }


    getTotalTimeCell(allLectures){
        let totalSeconds = this.getTotalSeconds(allLectures);

        const date = new Date(totalSeconds * 1000);
        date.setHours(date.getHours() - 1);
        const totalTime = date.toTimeString().split(' ')[0];

        return(
            <td className={"time-td"} data-seconds={totalSeconds}>
                {totalTime}
            </td>
        );
    }

    getTotalSeconds(allLectures){
        let totalSeconds = 0;
        for (let lecture of allLectures){
            totalSeconds += lecture["attendanceTime"];
        }

        return totalSeconds;
    }

    getNumberOfAttendedLecturesCell(allLectures){
        const attendedLectures = this.getNumberOfAttendedLectures(allLectures);

        return(
            <td className={"attendance-td"}>
                {attendedLectures}
            </td>
        );
    }

    getNumberOfAttendedLectures(allLectures){
        let attendedLectures = 0;
        for (let lecture of allLectures){
            if(lecture["attendanceTime"] > 0)
                attendedLectures++;
        }

        return attendedLectures;
    }

    async loadState() {

            let reqHeader = new Headers();
            reqHeader.append('Content-Type', 'application/json');

            const initObject = {
                method: 'POST', headers: reqHeader,
            };

            const userRequest = new Request('curles-backend/curlHandler.php', initObject);

            const fetchData = await fetch(userRequest)
                .then(function (response) {
                    return response.json();
                })
                .then(function (data) {
                    return data;
                })
                .catch(function (err) {
                    console.log("Something went wrong!", err);
                });


            this.props.onJsonLoaded(fetchData["graphStats"]);
            this.setState({timeSort: this.state.timeSort,
                                countSort: this.state.countSort,
                                nameSort: this.state.nameSort,
                                search: this.state.search,
                                records: fetchData["records"]});
    }



}

export default AttendanceTable;