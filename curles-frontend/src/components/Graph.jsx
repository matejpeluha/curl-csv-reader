import React, {Component} from 'react'
import { Bar } from 'react-chartjs-2';
import "../components-css/graph.css"

class Graph extends Component {
    constructor(props) {
        super(props);
        this.chartReference = React.createRef();
    }


    getLabels(){
        const stats = this.props.stats;
        const labels = [];
        for (let index = 0; index < stats.length; index++){
            let lectureCount = index + 1;
            const date = stats[index]["date"]
            labels.push([lectureCount + ".prednáška", date])
        }

        return labels;
    }

    getData(){
        const stats = this.props.stats;
        const data = [];
        for (let lecture of stats){
            data.push(lecture["count"])
        }

        return data;
    }

    render() {
        const data = {
            labels: this.getLabels(),
            datasets: [
                {
                    label: 'Počet študentov',
                    data: this.getData(),
                    fontColor: "white",
                    backgroundColor: "#006767"
                }
            ]
        };
        const options = {
            maintainAspectRatio: false,
            scales: {
                yAxes: [
                    {
                        ticks: {
                            fontColor: "white",
                            stepSize: 10,
                            beginAtZero: true,
                            borderColor: "white",
                        },
                        gridLines: {
                            color: "white"
                        }
                    }
                ],
                xAxes: [
                    {
                        ticks: {
                            fontColor: "white",
                            stepSize: 1,
                            borderColor: "white",
                        },
                        gridLines: {
                            color: "white"
                        }
                    }
                ],
            },
            legend: {
                labels: {
                    fontColor: "white"
                }
            }
        }
        return (
            <div id={"graph-container"}>
                <Bar
                    data={data}
                    width={100}
                    height={300}
                    options={options}
                />
            </div>
        )
    }

}


export default Graph