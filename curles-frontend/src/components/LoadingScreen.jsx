import React, {Component} from 'react'
import "../components-css/loadingScreen.css"

class LoadingScreen extends Component {
    render() {
        return (
            <div className={"loading " + this.getVisibilityClass()}>
                <div className='uil-ring-css'>
                    <div>

                    </div>
                </div>
            </div>
        );
    }

    getVisibilityClass(){
        if(this.props.active === false)
            return "hide-loading";

        return "";
    }
}

export default LoadingScreen;
