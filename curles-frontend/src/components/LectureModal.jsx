import React, {Component} from 'react';
import "../components-css/lectureModal.css"

class LectureModal extends Component {

    constructor(props) {
        super(props);

        window.onclick = this.windowClickHandle;
    }

    windowClickHandle = event =>{
        if(this.props["modal-info"]["isOpened"] && event.target.id === "modal-info")
            this.props.onCloseModal();
    }

    render() {

        const showModalClass = this.getShowModalClass();

        return (
            <div className="w3-container" >
                <div id="modal-info" className={"w3-modal " + showModalClass}>
                    <div id={"modal-content"} className="w3-modal-content w3-animate-top w3-card-4">
                        {this.getModalHeader()}
                        {this.getModalBody()}
                        <footer className="w3-container teal-color" />
                    </div>
                </div>
            </div>



        );
    }

    getShowModalClass(){
        if(this.props["modal-info"]["isOpened"] === true)
            return "opened-modal";
        else
            return "closed-modal";
    }

    getModalHeader(){
        return(
            <header className="w3-container teal-color">
                            <span onClick={this.props.onCloseModal}
                                  className="w3-button w3-display-topright">&times;</span>
                <h1>{this.props["modal-info"]["personName"]}</h1>
            </header>
        );
    }

    getModalBody(){
        const modalInfo = this.props["modal-info"];
        const lecture = modalInfo["lecture"];
        const time = (lecture["attendanceTime"] / 60 ).toFixed(2);
        const endTime = lecture["lectureEndTime"].split(" ")[1];

        return(
            <div className="w3-container modal-body">
                <h2>{modalInfo["lectureIndex"] + ".prednáška, " + lecture["date"]}</h2>
                <div className={"modal-time-stats-container"}>
                    <div className={"modal-time-stats-white"}>
                        <div className={"modal-time-stats"}>
                            <p className={"end-time-p"}>{"Koniec prednášky: " + endTime}</p>
                            <p className={"time-p"}>{"Čas v prednáške: " + time + " minút"}</p>
                        </div>
                    </div>
                </div>

                {this.getActionsTable(lecture["actions"])}
            </div>
        );
    }

    getTime(seconds){
        const date = new Date(seconds * 1000);
        date.setHours(date.getHours() - 1);
        return  date.toTimeString().split(' ')[0];
    }

    getActionsTable(allActions){
        return(
          <table id={"modal-table"}>
              <tbody>
                {allActions.map((action, key) => this.getActionRow(action, key))}
              </tbody>
          </table>
        );
    }

    getActionRow(action, key){
        const time = action["time"].split(" ")[1];
        const type = action["type"];

        return(
          <tr key={key}>
              <td className={"type-td-modal"}>
                  {type + ":"}
              </td>
              <td className={"time-td-modal"}>
                  {time}
              </td>
          </tr>
        );
    }

}

export default LectureModal;
